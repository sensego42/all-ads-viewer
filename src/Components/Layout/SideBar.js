import React from "react";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";
import "../../App.css";

export default (props) => {
  return (
    <Menu
      width={200}
      pageWrapId="globalComponent"
      outerContainerId={"App"}
      isOpen={props.IsOpen}
      disableCloseOnEsc
      noOverlay
      onStateChange={(isMenuOpen) => props.changeIsOpen(isMenuOpen.isOpen)}
    >
      <h2>Sensego</h2>
      <Link to="/home">Trending now !</Link>
      <Link to="/sectors">Sectors</Link>
      <Link to="/brands">Brands</Link>
      <Link to="/ads">Ads</Link>
    </Menu>
  );
};
