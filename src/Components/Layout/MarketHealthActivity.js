import React from "react";
import { VictoryBar, VictoryChart } from "victory";
import MainSection from "./MainSection";

const MarketHealthActivity = (props) => {
  return (
    <MainSection sectionHeader="Market health & activity">
      <div className="stat-container">
        <div className="stat-header">Incoming ads</div>
        <VictoryChart>
          <VictoryBar data={props.data} x="sector" y="nb_total" />
        </VictoryChart>
      </div>
      <div className="stat-container">
        <div className="stat-header">Ads activity ratio</div>
        <VictoryChart>
          <VictoryBar data={props.data} x="sector" y="nb_total" />
        </VictoryChart>
      </div>
      <div className="stat-container">
        <div className="stat-header">Ads volumes</div>
        <VictoryChart>
          <VictoryBar data={props.data} x="sector" y="nb_total" />
        </VictoryChart>
      </div>
    </MainSection>
  );
};

export default MarketHealthActivity;
