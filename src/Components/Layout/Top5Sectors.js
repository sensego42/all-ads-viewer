import React from "react";
import { VictoryBar, VictoryChart, VictoryTheme, VictoryPie } from "victory";

const renderData = (data) => {
  let result = [];
  data.map((value, i) => {
    result.push({ x: i });
    return [
      { x: 1, y: 2, label: "one" },
      { x: 2, y: 3, label: "two" },
      { x: 3, y: 5, label: "three" },
    ];
  });
};

const Top5Sectors = (props) => {
  return (
    <div className="main-section">
      <div className="section-header">Top 5 sectors</div>
      <div className="section-main">
        <div className="stat-container">
          <div className="stat-header">Top 5 sectors</div>
          <VictoryChart theme={VictoryTheme.graycale}>
            <VictoryBar data={props.topSector} x="sector" y="nb_total" />
          </VictoryChart>
        </div>
        <div className="stat-container">
          <div className="stat-header">Pie chart</div>
          <VictoryPie
            colorScale={["tomato", "orange", "gold", "cyan", "navy"]}
            innerRadius={100}
            labelRadius={120}
            data={renderData(props.topSector)}
          />
        </div>
      </div>
    </div>
  );
};

export default Top5Sectors;
