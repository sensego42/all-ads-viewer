import React from "react";

const MainSection = (props) => {
  console.log("props", props);
  return (
    <div className="main-section">
      <div className="section-header">{props.sectionHeader}</div>
      <div className="section-main">{props.children}</div>
    </div>
  );
};

export default MainSection;
