import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import "../../App.css";
import Home from "../Pages/Home";
import Sectors from "../Pages/Sectors";
import NotFound from "../Pages/NotFound";

class Main extends Component {
  state = {};
  render() {
    let className = "main";
    if (this.props.isOpen) {
      className += " isOpen";
    }
    return (
      <div id="main" className={className}>
        <Switch>
          <Route
            exact
            path={["/home", "/"]}
            render={(props) => (
              <Home isOpen={this.props.isOpen} data={this.props.data} />
            )}
          />
          <Route
            path="/sectors"
            render={(props) => <Sectors isOpen={this.props.menuIsOpen} />}
          />
          <Route component={NotFound} />
        </Switch>
      </div>
    );
  }
}

export default Main;
