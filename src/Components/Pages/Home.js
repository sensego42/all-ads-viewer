import React, { Component } from "react";
import "./Home.css";
import MarketHealthActivity from "../Layout/MarketHealthActivity";
import Top5Sectors from "../Layout/Top5Sectors";
import TrendingCampaigns from "../Layout/TrendingCampaigns";
import Top5Brands from "../Layout/Top5Brands";
import MarketForecast from "../Layout/MarketForecasts";

class Home extends Component {
  state = {
    topSector: null,
  };

  componentWillMount() {
    let topSector = this.props.data.slice();
    topSector.sort((a, b) => (a.nb_total < b.nb_total ? 1 : -1));
    topSector = topSector.slice(0, 4);
    this.setState({ topSector });
    console.log(topSector);
  }

  render() {
    let className = "";
    if (!this.props.isOpen) {
      className += " isClosed";
    }
    return (
      <div className="home-page">
        <div className="page-header">
          <div className={"header-mapper" + className}>
            <span className="mapper-logo">Adster</span> | Web trends
          </div>
          <div className="header-banner">
            <img
              className="banner-img"
              alt="gif dnaChain"
              src={require("../../Images/dnaChain.gif")}
            />
            <p className="banner-text">
              Anim proident proident aliquip reprehenderit labore ut...
            </p>
          </div>
        </div>
        <div className="page-main">
          <MarketHealthActivity data={this.props.data} />
          <Top5Sectors topSector={this.state.topSector} />
          <TrendingCampaigns />
          <Top5Brands />
          <MarketForecast />
        </div>
      </div>
    );
  }
}

export default Home;
