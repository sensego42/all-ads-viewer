import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";

import Main from "./Components/Layout/Main";
import SideBar from "./Components/Layout/SideBar";

// import Data from "./Data/All_Ads.json";
import Data from "./Data/Ads_Aggregated_Stats_May-19-2020.json";

class MainApplication extends Component {
  state = {
    menuIsOpen: window.innerWidth > 600 ? true : false,
  };
  data = Data.slice(0, 18);

  render() {
    console.log("size", window.innerWidth);
    return (
      <div id="mainApplication">
        <BrowserRouter>
          <SideBar
            changeIsOpen={(menuIsOpen) => this.setState({ menuIsOpen })}
            IsOpen={this.state.menuIsOpen}
          />
          <Main isOpen={this.state.menuIsOpen} data={this.data} />
        </BrowserRouter>
      </div>
    );
  }
}

export default MainApplication;
