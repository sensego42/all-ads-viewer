import React from "react";
import "./App.css";
import MainApplication from "./MainApplication";

function App() {
  return (
    <div className="App" id="App">
      <MainApplication />
    </div>
  );
}

export default App;
